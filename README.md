# Advent of  Code

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like. People use them as a speed contest, interview prep, company training, university coursework, practice problems, or to challenge each other.

You don't need a computer science background to participate - just a little programming knowledge and some problem solving skills will get you pretty far. Nor do you need a fancy computer; every problem has a solution that completes in at most 15 seconds on ten-year-old hardware.

If you'd like to support Advent of Code, you can do so indirectly by helping to share it with others, or directly via PayPal or Coinbase.

Advent of Code is a registered trademark in the United States.

Web site: [Advent of Code](https://adventofcode.com)

---

- [Advent of  Code](#advent-of--code)
  - [Challenge December 1st](#challenge-december-1st)
    - [Problem description 1st](#problem-description-1st)
    - [Solution 1st problem](#solution-1st-problem)
  - [Challenge December 2nd](#challenge-december-2nd)
    - [Description problem 2nd](#description-problem-2nd)
    - [Solution 2nd problem](#solution-2nd-problem)
  - [Challenge December 3rd](#challenge-december-3rd)
    - [Problem description 3rd](#problem-description-3rd)
    - [Solution 3rd problem](#solution-3rd-problem)
  - [Challenge December 4th](#challenge-december-4th)
    - [Problem description 4th](#problem-description-4th)
    - [Solution 4th problem](#solution-4th-problem)

---

## Challenge December 1st

### Problem description 1st

### Solution 1st problem

``` Python
def readInput():
    with open("defis/inputs/inputDec01.txt") as inputFile:
        return inputFile.readlines()
        
def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()
    myInput = [int(n) for n in myTextInput]

    print("*** Part One ***")
    print (findProductOfSumEqual(myInput, 2020))

    input("Hit any key but not too hard !")

    print("*** Part Two ***")

    for n in myInput:
        result = (findProductOfSumEqual(myInput, 2020-n))
        if result != 0:
            print(f"Third: {n} Result: {result * n}")
            break
             
    
    print("\n\n\n--- Fin du défi ---\n")

def findProductOfSumEqual(myInput, numberToReach):
    startIndex = 1
    result = 0
    nbItem = len(myInput)

    while startIndex < nbItem - 2 and result == 0:
        for x in myInput[startIndex::]:
            print(
                f"First: {myInput[startIndex - 1]} Second: {x} Sum: {myInput[startIndex - 1]+x}")
            if myInput[startIndex - 1] + x == numberToReach:
                result = myInput[startIndex - 1] * x
                print(f"Computed result: {result}")
                break
        startIndex += 1
    return result
    
```

---

## Challenge December 2nd

### Description problem 2nd

### Solution 2nd problem

---

## Challenge December 3rd

### Problem description 3rd

### Solution 3rd problem

---

## Challenge December 4th

### Problem description 4th

### Solution 4th problem

---

import re
import math


def readInput():
    with open("defis/inputs/inputDec09.txt") as inputFile:
        return inputFile.readlines()


def isValidNumber(pInputText, pNumberToValidate):
    #[(lambda x: x*x)(x) for x in range(10)]
    inputInteger = [(lambda x: int(x))(x) for x in pInputText]
    rc = False

    for i in range(len(inputInteger)-1):
        baseNumber = inputInteger[i]
        for x in inputInteger[i+1::]:
            print(f"base : {baseNumber} x: {x} somme: {baseNumber + x}")
            if baseNumber + x == pNumberToValidate:
                rc = True
                return rc
    return rc


def findContiguousSet(pInputText, pNumberToValidate):
    #[(lambda x: x*x)(x) for x in range(10)]
    inputInteger = [(lambda x: int(x))(x) for x in pInputText]
    rc = []

    pos = 0
    # pour tous les items du vecteur
    while pos < len(inputInteger):
        # je cumule les items tant que la somme ne dépasse pas ou
        rc = []
        rc.append(inputInteger[pos])
        i = 0
        while sum(rc) < pNumberToValidate:
            i+= 1
            rc.append(inputInteger[pos+i])
        if sum(rc) == pNumberToValidate:
            break
        pos += 1
    return rc

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    startIndex=0
    nbPreambule=25
    
    for i in range(nbPreambule, len(myTextInput)-nbPreambule):
        if not isValidNumber(myTextInput[startIndex:(startIndex+nbPreambule)], int(myTextInput[i])):
            result = int(myTextInput[i])
            break
        else:
            print(myTextInput[i] + " OK")
            startIndex += 1

    print(f"nb: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")

    myList = findContiguousSet(myTextInput, result)
    print(f"Nb distinct answers by all: {myList}\n")

    print(f"Min: {min(myList)} Max: {max(myList)} Sum: {min(myList)+max(myList)}")
    print("\n\n\n--- Fin du défi ---\n")


if __name__ == "__main__":
    execute()

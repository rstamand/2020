def printPythonFile(pFilename):
    print(pFilename)
    print("\n\n--- Début de fichier ---\n\n")
    with open(pFilename) as reader:
        print(reader.read())
    print("\n\n--- Fin de fichier ---\n\n")
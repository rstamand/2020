import re
import math
import json

def readInput():
    with open("defis/inputs/inputDec06.txt") as inputFile:
        return inputFile.readlines()

def getAllGroups(pTextInput):
    groups = [[]]
    i = 0
    for answer in pTextInput:
        if len(answer) > 1:
            groups[i].append(answer.replace("\n",""))
        else:
            groups.append([])
            i += 1
    return groups

def findAnswerDistinctEqualYesInGroup(pGroup):
    pass

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    allGroups = getAllGroups(myTextInput)

    allGroupsAnswers = []
    for groups in allGroups:
        groupAnswers = ""
        for group in groups:
                groupAnswers = "".join(set(groupAnswers + group))
        
        allGroupsAnswers.append(groupAnswers)

    result = 0
    for groupAnswers in allGroupsAnswers:
        result += len(groupAnswers)

    print(f"Nb distinct answers: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")
    
    allGroupsAnswers = []
    for groups in allGroups:
        groupAnswers = groups[0]
        for group in groups[1::]:
            groupAnswers = "".join(set(groupAnswers).intersection(group))
        allGroupsAnswers.append(groupAnswers)

    result = 0
    for groupAnswers in allGroupsAnswers:
        result += len(groupAnswers)

    print(f"Nb distinct answers by all: {result}\n")

    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()

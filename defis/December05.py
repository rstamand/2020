import re
import math
import json

def readInput():
    with open("defis/inputs/inputDec05.txt") as inputFile:
        return inputFile.readlines()

def extractRows(pInputFile):
    return [x[0:6]  for x in pInputFile]


def extractColumns(pInputFile):
    return [x[7:10] for x in pInputFile]


def computeSeat(pSeat, pNbRow=128, pNbColumn=8):
    return ((computeRow(pSeat, pNbRow) * 8) + computeColumn(pSeat, pNbColumn))

def seatDecimalValue(pSeat):
    return int(pSeat[0:7].replace("B", "1").replace("F", "0") + \
        pSeat[7:10].replace("R", "1").replace("L", "0"), 2)
     

def getLastSeat(pInputText):
    return pInputText[len(pInputText)-1]


def computeRow(pSeat, pNbRow):
    return int(pSeat[0:7].replace("B", "1").replace("F", "0"), 2)


def computeColumn(pSeat, pNbColumn):
    return int(pSeat[7:10].replace("R", "1").replace("L","0"),2)

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    myTextInput.sort(key=computeSeat) # make the B first

    lastSeat = getLastSeat(myTextInput)
    print(lastSeat)
    result = computeSeat(lastSeat,128,8)

    print(f"Max seat number: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")
    
    previousVal = myTextInput[0]
    for seat in myTextInput[1::]:
        if (seatDecimalValue(seat) - seatDecimalValue(previousVal)) > 1:
            print(f"Available seat between {previousVal} and {seat}")
            print(computeSeat(seat)-1)
            break

        else:
            print(f"{seatDecimalValue(previousVal)} and {seatDecimalValue(seat)}")
            previousVal = seat



    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()

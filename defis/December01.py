# todo: Implanter la multiplication via la librairie math avec la fonction prod []

if __name__ != "__main__":
    from .util.printing import printPythonFile as printCode

def hello():
    print("Défi du premier décembre 2020")
    printCode(__file__)

def readInput():
    with open("defis/inputs/inputDec01.txt") as inputFile:
        return inputFile.readlines()
        
def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()
    myInput = [int(n) for n in myTextInput]

    print("*** Part One ***")
    print (findProductOfSumEqual(myInput, 2020))

    input("Hit any key but not too hard !")

    print("*** Part Two ***")

    for n in myInput:
        result = (findProductOfSumEqual(myInput, 2020-n))
        if result != 0:
            print(f"Third: {n} Result: {result * n}")
            break
             
    
    print("\n\n\n--- Fin du défi ---\n")

def findProductOfSumEqual(myInput, numberToReach):
    startIndex = 1
    result = 0
    nbItem = len(myInput)

    while startIndex < nbItem - 2 and result == 0:
        for x in myInput[startIndex::]:
            print(
                f"First: {myInput[startIndex - 1]} Second: {x} Sum: {myInput[startIndex - 1]+x}")
            if myInput[startIndex - 1] + x == numberToReach:
                result = myInput[startIndex - 1] * x
                print(f"Computed result: {result}")
                break
        startIndex += 1
    return result


if __name__ == "__main__":
    execute()

import re
import math

def readInput():
    with open("defis/inputs/inputDec07.txt") as inputFile:
        return inputFile.readlines()

def getAllBagsContaining(pTextInput, pBagName,  pAllBags, pFilAriane=""):

    pFilAriane += "-->"+pBagName
    print(pFilAriane)

    for bagSpecs in pTextInput:
        bag, contents = bagSpecs.split(" contain ")[0:2]
        if contents.find(pBagName) > 0:
            pAllBags.append(bag)
            for content in (set(contents[:-2].split(", "))):
                contentBag = re.match(
                    "^([0-9]) (.*)", content)[2]
                if contentBag != pBagName:
                    pAllBags = getAllBagsContaining(
                        pTextInput, bag, pAllBags, pFilAriane)
            pAllBags = getAllBagsContaining(pTextInput, bag, pAllBags, pFilAriane)

    return pAllBags

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    allBags = []
    allBags = getAllBagsContaining(
        myTextInput, "shiny gold bag", allBags)

    allBags.sort()
    print(len(set(allBags)))
    print(set(allBags))
    print(allBags)
    result = 0
    print(f"Nb Bag that can contain at least one shiny gold bag: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")
    
    print(f"Nb distinct answers by all: {result}\n")

    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()

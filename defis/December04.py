import re
import math
import json

def readInput():
    with open("defis/inputs/inputDec04.txt") as inputFile:
        return inputFile.readlines()

def createJson(pInput):
    myJson = '{\n"Credentials":[{'

    for x in pInput:
        if len(x) <= 1:
            myJson += '},\n{'
        else:
            myJson += '"'+x.replace(" ", ',').replace(":", '":"').replace(",",'","').replace("\n",'"')
    myJson += "}]\n}"
    return myJson.replace('""', '","')

def getValidJson(pTextInput):
    myInputJson = createJson(pTextInput)
    
    decoded_json = json.loads(myInputJson)
    return decoded_json

def isValidCredential(pCredentialDict):
    try:
        return (len(pCredentialDict["byr"]) > 0 and
                len(pCredentialDict["iyr"]) > 0 and
                len(pCredentialDict["eyr"]) > 0 and
                len(pCredentialDict["hgt"]) > 0 and
                len(pCredentialDict["hcl"]) > 0 and
                len(pCredentialDict["ecl"]) > 0 and
                len(pCredentialDict["pid"]) > 0)
    except KeyError as e:
        return False


def isValidYear(yr, minYear, maxYear):
    if len(yr) == 4 and re.match("[0-9]{4}", yr):
        if int(yr) >= minYear and int(yr) <= maxYear:
            return True
        else:
            return False

def validBirthYear(byr):
    if isValidYear(byr,1920,2002):
        return True
    return False

def validIssueYear(iyr):
    if isValidYear(iyr, 2010, 2020):
        return True
    return False

def validExpirationYear(eyr):
    if isValidYear(eyr, 2020, 2030):
        return True
    return False

def validHeight(hgt):
    matching = re.match("([0-9]{1,3})(cm)", hgt)
    if matching:
        nb = int(matching.group(1))
        unit = matching.group(2)
        if (unit == "cm" and nb >= 150 and nb <= 193):
            return True
        else:
            return False
    
    matching = re.match("([0-9]{1,3})(in)", hgt)
    if matching:
        nb = int(matching.group(1))
        unit = matching.group(2)
        if (unit == "in" and nb >= 59 and nb <= 76):
            return True

    return False

def validHairColor(hcl):
    if len(hcl) == 7 and re.match("#[0-9a-f]{6}", hcl):
        return True

    return False

def validEyeColor(ecl):
    if len(ecl) == 3 and re.match("amb|blu|brn|gry|grn|hzl|oth", ecl):
        return True
    return False

def validPassportID(pid):
    if len(pid) == 9 and re.match("[0-9]{9}", pid):
        return True
    return False

def isValidCredentialPartTwo(pCredentialDict):
    try:
        return (validBirthYear(pCredentialDict["byr"]) and
                validIssueYear(pCredentialDict["iyr"]) and
                validExpirationYear(pCredentialDict["eyr"]) and
                validHeight(pCredentialDict["hgt"]) and
                validHairColor(pCredentialDict["hcl"]) and
                validEyeColor(pCredentialDict["ecl"]) and
                validPassportID(pCredentialDict["pid"]))

    except KeyError as e:
        return False

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    credentials = getValidJson(myTextInput)
    print(credentials)

    result = 0
    for cred in credentials["Credentials"]:
        if isValidCredential(cred):
            result += 1 

    print(f"Nb valid credentials: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")
    
    result = 0
    for cred in credentials["Credentials"]:
        if isValidCredentialPartTwo(cred):
            result += 1

    print(f"Nb valid credentials: {result}\n")

    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()

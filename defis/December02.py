import re

def readInput():
    with open("defis/inputs/inputDec02.txt") as inputFile:
        return inputFile.readlines()

def splitLine(pInput):
    parts = pInput.split(":")
    password = parts[1]
    parts2 = parts[0].split(" ")
    char = parts2[1]
    parts3 = parts2[0].split("-")
    minOccurrence = int(parts3[0])
    maxOccurrence = int(parts3[1])
    return [minOccurrence,maxOccurrence,char,password]

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    print("*** Part One ***")

    nbMatchs = 0
    for item in myTextInput:
        params = splitLine(item)
        nbOccurrences = re.findall(params[2], params[3])
        if len(nbOccurrences) >= int(params[0]) and len(nbOccurrences) <= int(params[1]):
            nbMatchs += 1

    print (f"Nb matches: {nbMatchs}\n")

    input("Hit any key but not too hard !")

    print("*** Part Two ***")
    
    nbMatchs = 0
    for item in myTextInput:
        params = splitLine(item)
        if (params[3][int(params[0])] == params[2]) ^ (params[3][int(params[1])] == params[2]):
            nbMatchs += 1
    print(f"Nb matches: {nbMatchs}\n")

    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()

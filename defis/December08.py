import re
import math


accumulateur = 0 # global accumulator

def readInput():
    with open("defis/inputs/inputDec08.txt") as inputFile:
        return inputFile.readlines()

def getInstructions(pTextInput):

    instructions = []
    for instruction in pTextInput:
        operation = instruction[:-1].split(" ")[0]
        argument = instruction[:-1].split(" ")[1]
        instructions.append([operation, argument, False])

    return instructions

def executeInstruction(pInstructions, pInstNo):
    global  accumulateur
    
    if pInstNo < 0:
        return -1
    else:
        if pInstructions[pInstNo][2] == True:
            return -1
        if pInstructions[pInstNo][0] == "nop":
            pInstructions[pInstNo][2] = True
            return pInstNo + 1
        else:
            if pInstructions[pInstNo][0] == "acc":
                accumulateur = accumulateur + int(pInstructions[pInstNo][1])
                pInstructions[pInstNo][2] = True
                return pInstNo + 1
            else:
                if  pInstructions[pInstNo][0] == "jmp":
                    pInstructions[pInstNo][2] = True
                    return pInstNo + int(pInstructions[pInstNo][1])

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()

    instructions = getInstructions(myTextInput)


    nextInstruction = 0
    while (nextInstruction >= 0 and nextInstruction < len(instructions)):
        nextInstruction = executeInstruction(instructions, nextInstruction)
         

    print(len(instructions))
    print(nextInstruction)
 
    result = accumulateur
    print(f"Accumulateur: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")

    nextInstruction = 0
    while nextInstruction < len(instructions):
        nextInstruction = 0
        while (nextInstruction >= 0 and nextInstruction < len(instructions)):
            nextInstruction = executeInstruction(instructions, nextInstruction)



    print(f"Nb distinct answers by all: {result}\n")

    print("\n\n\n--- Fin du défi ---\n")


if __name__ == "__main__":
    execute()

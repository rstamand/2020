import re
import math

def readInput():
    with open("defis/inputs/inputDec03.txt") as inputFile:
        return inputFile.readlines()

def createPlayGround(pInput, nbCopy):
    nbLine = len(pInput)
    nbInitialColumn = len(pInput[0])
    nbRepetition = round(nbLine * nbCopy / nbInitialColumn) + 10
    playGround = []
    for x in pInput:
        playGround.append(x.replace("\n", "")*nbRepetition)
    return playGround

def findNbTrees(pTextInput, down=1, right=3):
    playGround = createPlayGround(pTextInput, right)
    i = 0
    trail = ""
    for line in playGround[::down]:
        trail += line[i]
        i += right
    result = len(re.findall("#", trail))
    return result

def execute():
    print("\n\n\n--- Début du défi ---\n")

    myTextInput = readInput()


    print("*** Part One ***")
    slopes = [[1, 3]]

    result = findNbTrees(myTextInput)
    print(f"Nb trees: {result}\n")

    input("Hit <ENTER> but not too hard !")

    print("*** Part Two ***")
    slopes = [[1,1],[1,3],[1,5],[1,7],[2,1]]
    results = [] 
        
    for slope in slopes:
        results.append(findNbTrees(myTextInput, slope[0], slope[1]))

    print(f"Nb trees: {results}\n")
    print(math.prod(results))
    print("\n\n\n--- Fin du défi ---\n")

if __name__ == "__main__":
    execute()
